package com.recruitment.labyrinth;

import java.util.List;
import com.recruitment.labyrinth.exception.ClosedDoorException;
import com.recruitment.labyrinth.exception.DoorAlreadyClosedException;
import com.recruitment.labyrinth.exception.IllegalMoveException;
import com.recruitment.labyrinth.player.Player;
import com.recruitment.labyrinth.room.RoomLink;

public class Labyrinth {
	
	private List<RoomLink> roomLinks;
	private Player player;
	private LabyrinthManager labyrinthManager;

	public Labyrinth(String... roomLinksString) {
		player =new Player();
		labyrinthManager = new LabyrinthManager();
		roomLinks = labyrinthManager.createLabyrinth(roomLinksString);
	}
		
	public void popIn(String roomName) {
		player.popIn(roomName);	
	}

	public void walkTo(String roomName) throws IllegalMoveException,ClosedDoorException {
		player.walkTo(roomName,this.roomLinks);
	}
	
	public void closeLastDoor() throws DoorAlreadyClosedException{
		player.closeLastDoor(roomLinks);
	}

	public String readSensors() {
		return labyrinthManager.printPath(player.getFollowedRooms());
	}

}
