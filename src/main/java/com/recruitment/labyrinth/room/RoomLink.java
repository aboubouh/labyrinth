package com.recruitment.labyrinth.room;

import com.recruitment.labyrinth.door.Door;

public class RoomLink {
	
	private Room source;
	private Room destination;
	private Door door;
	
	public RoomLink(Room source,Room destination,Door door){
		this.source=source;
		this.destination = destination;
		this.door=door;
	}
	public Room getSource() {
		return source;
	}
	
	public Room getDestination() {
		return destination;
	}
	
	public Door getDoor() {
		return door;
	}
}
