package com.recruitment.labyrinth.enums;

public enum DoorType {

	NORMAL("|"),
	WITHSENSOR("$");
	
	private String representation;
	
	DoorType(String representation){
		this.representation=representation;
	}
	
}
