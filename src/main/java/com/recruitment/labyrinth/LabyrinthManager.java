package com.recruitment.labyrinth;

import static com.recruitment.labyrinth.door.DoorFactory.createDoor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.recruitment.labyrinth.door.Door;
import com.recruitment.labyrinth.enums.DoorType;
import com.recruitment.labyrinth.room.Room;
import com.recruitment.labyrinth.room.RoomLink;

public class LabyrinthManager {

	private static List<Room> ALL_ROOMS = new ArrayList<Room>();
	
	public List<RoomLink> createLabyrinth(String...roomLinksString){
		List<RoomLink> roomLinks = new ArrayList<RoomLink>();	
		for(String roomLinkString : roomLinksString){
			String[] rooms = null;
			Door door = null;
			if(roomLinkString.contains("$")){
				rooms=roomLinkString.split("\\$");
				door = createDoor(DoorType.WITHSENSOR);
			}
			if(roomLinkString.contains("|")){
				rooms= roomLinkString.split("\\|");
				door = createDoor(DoorType.NORMAL);
			}
			String roomFromName = rooms[0];
			String roomToName = rooms[1];
			Room roomFrom = addRoomIfDoesntExist(roomFromName);
			Room roomTo = addRoomIfDoesntExist(roomToName);
			RoomLink roomLink = new RoomLink(roomFrom,roomTo,door);
			roomLinks.add(roomLink);
		}
		return roomLinks;
	}
	
	public Room addRoomIfDoesntExist(String roomName){
		Optional<Room> optionalRoom = ALL_ROOMS.stream().filter(r ->roomName.equals(r.getName())).findFirst();
		Room room = null;
		if(!optionalRoom.isPresent()){
			room = new Room(roomName);
			ALL_ROOMS.add(room);
		}else{
			room = optionalRoom.get();
		}
		return room;
	}
	
	public List<Room> getAllRooms(){
		return ALL_ROOMS;
	}
	
	public Room getRoom(String roomName){
		Optional<Room> optionalRoom = ALL_ROOMS.stream().filter(r ->roomName.equals(r.getName())).findFirst();
		Room room =null;
		if(optionalRoom.isPresent()){
			room = optionalRoom.get();
		}
		return room;
	}
	
	public boolean checkIfRoomsInRelation(Room firstRoom,Room secondRoom,List<RoomLink> roomLinks){
		for(RoomLink roomLink : roomLinks){
			if((roomLink.getSource() == firstRoom && roomLink.getDestination()==secondRoom)
				||(roomLink.getSource() == secondRoom && roomLink.getDestination()==firstRoom)){
				return true;
			}
		}
		return false ;
	}
	
	public Door getDoorBetweenTwoRooms(Room firstRoom ,Room secondRoom,List<RoomLink> roomLinks){
		Door door =null;
		for(RoomLink roomLink : roomLinks){
			if((roomLink.getSource() == firstRoom && roomLink.getDestination()==secondRoom)
				||(roomLink.getSource() == secondRoom && roomLink.getDestination()==firstRoom)){
				door = roomLink.getDoor();
			}
		}
		return door;
	}
	

	public boolean checkIfDoorIsClosed(Room firstRoom,Room secondRoom,List<RoomLink> roomLinks){
		Door door = getDoorBetweenTwoRooms(firstRoom,secondRoom,roomLinks);
		if(door.isClosed()){
			return true;
		}
		return false;
	}

	public String printPath(Map<Room, Room> followedRooms) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<Room, Room> entry : followedRooms.entrySet()) {
			sb.append(entry.getKey().getName()+entry.getValue().getName()+";");
		}
		return sb.toString().substring(0,sb.toString().length()-1);
	}
	
	
	
}
