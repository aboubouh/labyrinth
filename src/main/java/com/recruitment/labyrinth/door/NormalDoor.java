package com.recruitment.labyrinth.door;

import com.recruitment.labyrinth.enums.DoorType;

public class NormalDoor extends Door{


	public NormalDoor(DoorType doorType){
		super(doorType);
	}
}
