package com.recruitment.labyrinth.door;

import com.recruitment.labyrinth.enums.DoorType;

public class Door {

	private DoorType doorType;
	private boolean closed;
	
	
	public Door(DoorType doorType){
		this.doorType=doorType;
		closed = false;
	}
	public Door(){
		closed = false;
	}
	
	public DoorType getDoorType() {
		return doorType;
	}
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}
	
}
