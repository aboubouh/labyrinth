package com.recruitment.labyrinth.door;

import com.recruitment.labyrinth.enums.DoorType;

public class DoorWithSensor extends Door {

	public DoorWithSensor(DoorType doorType){
		super(doorType);
	}
}
