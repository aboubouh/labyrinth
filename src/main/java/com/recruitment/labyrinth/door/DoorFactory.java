package com.recruitment.labyrinth.door;

import com.recruitment.labyrinth.enums.DoorType;

public class DoorFactory{

	public static Door createDoor(DoorType type){
		if(DoorType.NORMAL == type){
			return new NormalDoor(type);
		}
		else if(DoorType.WITHSENSOR == type){
			return new DoorWithSensor(type);
		}
		return null;
	}
	
}
