package com.recruitment.labyrinth.player;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.recruitment.labyrinth.LabyrinthManager;
import com.recruitment.labyrinth.door.Door;
import com.recruitment.labyrinth.enums.DoorType;
import com.recruitment.labyrinth.exception.ClosedDoorException;
import com.recruitment.labyrinth.exception.DoorAlreadyClosedException;
import com.recruitment.labyrinth.exception.IllegalMoveException;
import com.recruitment.labyrinth.room.Room;
import com.recruitment.labyrinth.room.RoomLink;

public class Player {

	private Room startPoint;
	private Room actualRoom;
	private Room previousRoom;
	private Map<Room,Room> followedRooms;
	private LabyrinthManager labyrinthManager;


	public Player(){
		followedRooms = new LinkedHashMap<Room,Room>();
		labyrinthManager = new LabyrinthManager();
	}
	
	public Map<Room, Room> getFollowedRooms() {
		return followedRooms;
	}
	
	public void popIn(String roomName) {
		Room startRoom = labyrinthManager.getRoom(roomName);
		this.startPoint = startRoom;
		this.actualRoom = startRoom;			
	}
	
	public void walkTo(String roomName,List<RoomLink> roomLinks){
		this.previousRoom = this.actualRoom;
		Room roomDestination = labyrinthManager.getRoom(roomName);
		if(roomDestination==null || !labyrinthManager.checkIfRoomsInRelation(this.actualRoom,roomDestination,roomLinks)){
			throw new IllegalMoveException();
		}
		if(labyrinthManager.checkIfDoorIsClosed(this.actualRoom, roomDestination,roomLinks)){
			throw new ClosedDoorException();
		}
		Door door = labyrinthManager.getDoorBetweenTwoRooms(this.actualRoom, roomDestination,roomLinks);
		if(door!=null && door.getDoorType()==DoorType.WITHSENSOR){
			this.getFollowedRooms().put(this.actualRoom, roomDestination);
		}
		
		this.actualRoom=roomDestination;	
	}

	public void closeLastDoor(List<RoomLink> roomLinks) {
		Door doorToBeClosed = labyrinthManager.getDoorBetweenTwoRooms(this.previousRoom, this.actualRoom,roomLinks);
		if(!doorToBeClosed.isClosed()){
			doorToBeClosed.setClosed(true);
		}else{
			throw new DoorAlreadyClosedException();
		}		
	}

	
}
